export enum LANDING_PAGE {
    ABOUTUS = 'aboutUs',
    CONTACTUS = 'contactUs',
    HELP = 'help',
    SERVICES = 'services'
}