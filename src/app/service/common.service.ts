import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Router} from '@angular/router';
import {AbstractControl, FormArray, FormGroup, Validators} from '@angular/forms';

@Injectable()
export class CommonService {

    isUserLoggedIn: boolean = false;

    constructor() {}

    logIn() {
        console.log('^^^^^^^^^ login submitted ^^^^^^^^^^^^^^^');
        this.isUserLoggedIn = true;
    }

    logOut() {
        console.log('^^^^^^^^^ logout reqeusted ^^^^^^^^^^^^^^^');
        this.isUserLoggedIn = false;
    }

    getUserStatus() {
        console.log('isUserLoggedIn : ', ''+this.isUserLoggedIn);
        return this.isUserLoggedIn;
    }
}