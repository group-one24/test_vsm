import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { createPopper } from '@popperjs/core';
import { CommonService } from 'src/app/service/common.service';

@Component({
  selector: 'app-pages-dropdown',
  templateUrl: './pages-dropdown.component.html',
})
export class PagesDropdownComponent implements OnInit {
  dropdownPopoverShow = false;
  @ViewChild('btnDropdownRef', { static: false }) btnDropdownRef: ElementRef;
  @ViewChild('popoverDropdownRef', { static: false })
  popoverDropdownRef: ElementRef;

  constructor(public commonService: CommonService) {}

  ngOnInit() {}

  toggleDropdown(event) {
    event.preventDefault();
    if (this.dropdownPopoverShow) {
      this.dropdownPopoverShow = false;
    } else {
      this.dropdownPopoverShow = true;
      this.createPoppper();
    }
  }

  logOut() {
    this.commonService.logOut();
  }

  createPoppper() {
    createPopper(
      this.btnDropdownRef.nativeElement,
      this.popoverDropdownRef.nativeElement,
      {
        placement: 'bottom-start',
      }
    );
  }
}
