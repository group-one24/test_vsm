import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CommonService } from 'src/app/service/common.service';
import { LANDING_PAGE } from 'src/app/model/common.model';

@Component({
  selector: 'app-auth-navbar',
  templateUrl: './auth-navbar.component.html',
})
export class AuthNavbarComponent implements OnInit {
  navbarOpen = false;

  constructor(private router: Router, public commonService: CommonService){}

  ngOnInit(): void {}

  setNavbarOpen() {
    this.navbarOpen = !this.navbarOpen;
  }

  goToSignUp() {
    this.router.navigate(['/register']);
  }

  goToSignIn() {
    this.router.navigate(['/login']);
  }

  goToHome() {
    this.router.navigate(['/home'], { queryParams: { page: 'main'} });
  }

  goToServices() {
    this.router.navigate(['/home'], { queryParams: { page: 'services'} });
  }

  goToHelp() {
    this.router.navigate(['/home'], { queryParams: { page: 'help' } });
  }

  goToAboutUs() {
    this.router.navigate(['/home'], { queryParams: { page: 'aboutUs' } });
  }

  goToContactUs() {
    this.router.navigate(['/home'], { queryParams: { page: 'contactUs' } });
  }

  isUserActive() {
    return this.commonService.getUserStatus();
  }

}
