import { Component, OnInit, ViewChild, ElementRef, AfterViewInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-landing',
  templateUrl: './landing.component.html',
})
export class LandingComponent implements OnInit, AfterViewInit {

  constructor(private route: ActivatedRoute, private router: Router) {}

  @ViewChild('aboutus')
  aboutus: ElementRef;

  @ViewChild('contactus')
  contactus: ElementRef;

  @ViewChild('home')
  home: ElementRef;

  @ViewChild('services')
  services: ElementRef;

  @ViewChild('help')
  help: ElementRef;

  openTab = 1;

  ngOnInit(): void {
  }

  ngAfterViewInit() {
    this.route.queryParams.subscribe(params => {
    if (params.page) {
      switch(params.page) {
        case 'main':
          this.goToHome();
          break;
        case 'aboutUs':
          this.goToAboutUs();
          break;
        case 'contactUs':
          this.goToContactUs();
          break;
        case 'help':
          this.goToHelp();
          break;
        case 'services':
          this.goToServices();
          break;
        default:
          this.goToHome();
          break;
      }
    }
  }
);

  }

  toggleTabs($tabNumber: number){
    this.openTab = $tabNumber;
  }

  goToAboutUs(): void {
    this.aboutus.nativeElement.scrollIntoView({behavior: 'smooth'});
  }
  goToContactUs(): void {
    this.contactus.nativeElement.scrollIntoView({behavior: 'smooth'});
  }
  goToHome(): void {
    this.home.nativeElement.scrollIntoView({behavior: 'smooth'});
  }
  goToServices(): void {
    this.services.nativeElement.scrollIntoView({behavior: 'smooth'});
  }
  goToHelp(): void {
    this.help.nativeElement.scrollIntoView({behavior: 'smooth'});
  }

  goToProfile() {
    this.router.navigate(['/profile']);
  }

  goToForm() {
    this.router.navigate(['/forms']);
  }

}
