import { Component, OnInit } from '@angular/core';
import { CommonService } from 'src/app/service/common.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
})
export class LoginComponent implements OnInit {
  constructor(public commonService: CommonService) {}

  ngOnInit(): void {}

  onSignInSubmit() {
    this.commonService.logIn();
  }
}
