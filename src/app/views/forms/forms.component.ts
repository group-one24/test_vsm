import { AfterViewInit, Component, ElementRef, OnInit, ViewChild } from '@angular/core';

@Component({
  selector: 'app-forms',
  templateUrl: './forms.component.html',
})
export class FormsComponent implements OnInit, AfterViewInit {
  constructor() {}

  @ViewChild('home')
  home: ElementRef;

  ngOnInit(): void {}

  ngAfterViewInit() {
    this.home.nativeElement.scrollIntoView({behavior: 'smooth'});
  }
}
