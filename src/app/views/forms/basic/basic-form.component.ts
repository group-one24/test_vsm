import { Component, OnInit } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';

@Component({
  selector: 'app-basic-form',
  templateUrl: './basic-form.component.html',
})
export class BasicFormComponent implements OnInit {
  constructor(private router: Router) {}

  ngOnInit(): void {}

  goToServices() {
    this.router.navigate(['/home'], { queryParams: { page: 'services'} });
  }
}
