import { AfterViewInit, Component, ElementRef, OnInit, ViewChild } from '@angular/core';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
})
export class ProfileComponent implements OnInit, AfterViewInit {
  constructor() {}

  @ViewChild('home')
  home: ElementRef;

  ngOnInit(): void {}

  ngAfterViewInit() {
    this.home.nativeElement.scrollIntoView({behavior: 'smooth'});
  }
}
